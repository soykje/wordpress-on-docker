.PHONY: install
## Install project
install:
	npm install
	# composer install

.PHONY: start
## Start the application for development
start:
	docker-compose up -d && npm start

.PHONY: build
## Build the application for production
build:
	npm run build

.PHONY: stop
# Stop the application
stop:
	docker-compose down


.DEFAULT_GOAL := help
.PHONY: help
help:
	@echo "Usage: make <COMMAND>"
	@echo
	@echo "Commands:"
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| awk -F '---' \
	-v ncol=$$(tput cols) \
	-v indent=12 \
	-v col_on="$$(tput setaf 6)" \
	-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s ", " "; \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}'
	@echo
