const path = require('path')

const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const StyleLintPlugin = require('stylelint-webpack-plugin')

module.exports = {
  entry: {
    main: [
      './src/js/main.js',
      './src/sass/main.scss'
    ]
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'js/[name].js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        include: path.resolve(__dirname, './src/js'),
        exclude: /node_modules/,
        use: ['babel-loader', 'eslint-loader']
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader',
          {
            loader: 'image-webpack-loader',
            options: {
              disable: true
            }
          }
        ]
      },
      // {
      //   test: /\.(woff|woff2|eot|ttf|otf)$/,
      //   use: [
      //     {
      //       loader: 'file-loader',
      //       options: {
      //         outputPath: 'fonts',
      //         publicPath: './../fonts'
      //       }
      //     }
      //   ]
      // }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'css/[name].css'
    }),
    new StyleLintPlugin({
      configFile: './stylelint.config.js',
      files: ['./src/sass/*.scss', './src/sass/*/*.scss'],
      syntax: 'scss'
    })
  ]
}
