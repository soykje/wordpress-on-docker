# Wordpress On Docker

## Prerequisites
You need to have Docker installed on your machine to run the project locally.
- Ubuntu https://docs.docker.com/install/linux/docker-ce/ubuntu/
- MacOS https://docs.docker.com/docker-for-mac/install/

## Install
To install dependencies (from Composer or Npm): `make install`

## Develop
To start theme development: `make start`
Then go to http://localhost:8000

## Build
To build theme for production: `make build`